package com.twuc.webApp.domain;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column
    private String name;
    @Column
    private Integer price;
    @Column
    private String unit;


    public Product() {
    }

    public Product(String name, Integer price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}