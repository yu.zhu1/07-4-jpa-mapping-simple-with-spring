package com.twuc.webApp.web;


import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ProductController {
    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @PostMapping("/api/products")
    ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest createProductRequest) {
        String name = createProductRequest.getName();
        Integer price = createProductRequest.getPrice();
        String unit = createProductRequest.getUnit();
        Product product = productRepository.save(new Product(name, price, unit));
        System.out.println(product.getId());
        productRepository.flush();
        return ResponseEntity.status(201).header("Location",String.format("http://localhost/api/products/%d",product.getId())).build();
    }


    @GetMapping("/api/products/{productId}")
    ResponseEntity getProduct(@PathVariable Long productId) {
        Optional<Product> byId = productRepository.findById(productId);
        if (byId.isPresent()) {
        Product product = byId.get();
            return ResponseEntity.status(200).body(product);
        } else return ResponseEntity.status(404).build();
    }

}